package com.rspn;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Stream;

public class FileService {

    public static List<Path> getProjects(String projectsRootPath) {
        try (Stream<Path> paths = Files.list(Path.of(projectsRootPath))) {
            return paths
                    .filter(p -> p.toFile().isDirectory())
                    .filter(p -> !p.toFile().getName().startsWith("."))
                    .toList();
        } catch (IOException e) {
            System.err.println(
                    "Error during reading projects from directory " + projectsRootPath + ". Error:" + e.getMessage());
            return List.of();
        }
    }
}
