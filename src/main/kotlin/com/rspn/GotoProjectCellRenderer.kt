package com.rspn

import com.intellij.ide.util.PsiElementListCellRenderer
import com.intellij.navigation.ItemPresentation
import com.intellij.navigation.NavigationItem
import com.intellij.openapi.util.Iconable
import com.intellij.openapi.util.text.StringUtil
import com.intellij.psi.PsiFileSystemItem
import com.intellij.ui.ColoredListCellRenderer
import com.intellij.ui.JBColor
import com.intellij.ui.SimpleTextAttributes
import javax.swing.JList

class GotoProjectCellRenderer internal constructor() : PsiElementListCellRenderer<PsiFileSystemItem>() {

    override fun getElementText(element: PsiFileSystemItem): String {
        return "getElementText"
    }

    override fun getContainerText(element: PsiFileSystemItem, name: String): String? {
        return "getContainerText"
    }

    override fun customizeNonPsiElementLeftRenderer(renderer: ColoredListCellRenderer<*>, list: JList<*>?, value: Any?, index: Int, selected: Boolean, hasFocus: Boolean): Boolean {
        return if (value !is NavigationItem) {
            false
        } else {
            val item = value as NavigationItem?
            val itemPresentation = item!!.presentation!!
            customizeCellContent(itemPresentation, renderer)
            true
        }
    }

    private fun customizeCellContent(itemPresentation: ItemPresentation, renderer: ColoredListCellRenderer<*>) {
        val projectNamePresentableText = itemPresentation.presentableText
        if (!StringUtil.isEmpty(projectNamePresentableText)) {
            renderer.append(projectNamePresentableText!!, SimpleTextAttributes(0, JBColor.BLACK))
            renderer.append(" (${itemPresentation.locationString})", SimpleTextAttributes(0, JBColor.GRAY), 10, 4)
        }
    }

    override fun getIconFlags() = Iconable.ICON_FLAG_READ_STATUS
}