package com.rspn.model

import com.rspn.GotoProjectCellRenderer
import com.rspn.GotoProjectNameProvider
import com.rspn.GotoProjectNavigationItem
import com.intellij.ide.util.PsiElementListCellRenderer
import com.intellij.ide.util.gotoByName.FilteringGotoByModel
import com.intellij.navigation.ChooseByNameContributor
import com.intellij.navigation.NavigationItem
import com.intellij.openapi.fileTypes.FileType
import com.intellij.openapi.project.DumbAware
import com.intellij.openapi.project.Project
import com.intellij.psi.PsiFile

class GotoProjectModel internal constructor(project: Project)
    : FilteringGotoByModel<FileType>(project, arrayOf<ChooseByNameContributor>(GotoProjectNameProvider())), DumbAware {

    override fun acceptItem(item: NavigationItem?): Boolean {
        return item is GotoProjectNavigationItem || super.acceptItem(item)
    }

    override fun filterValueFor(item: NavigationItem): FileType? {
        return (item as? PsiFile)?.fileType
    }

    override fun getPromptText() = "Enter project name"

    override fun getCheckBoxName(): String? {
        return null
    }

    override fun getNotInMessage() = "not in message"

    override fun getNotFoundMessage() = "not found message"

    override fun loadInitialCheckBoxState() = true

    override fun saveInitialCheckBoxState(state: Boolean) {}

    override fun getListCellRenderer(): PsiElementListCellRenderer<*> {
        return GotoProjectCellRenderer()
    }

    override fun getFullName(element: Any): String? {
        return this.getElementName(element)
    }

    override fun getSeparators(): Array<String> {
        return arrayOf("/", "\\")
    }

    override fun willOpenEditor() = true
}