package settings

import com.intellij.openapi.components.PersistentStateComponent
import com.intellij.openapi.components.State
import com.intellij.openapi.components.Storage
import com.intellij.openapi.components.service
import com.intellij.util.xmlb.XmlSerializerUtil


@State(
    name = "ProjectOpenerSettings",
    storages = [Storage("project-opener.settings.xml")]
)
class ProjectOpenerSettingsState : PersistentStateComponent<ProjectOpenerSettingsState> {
    var projectsRootPaths: List<String> = listOf()
    var forceOpenNewWindows: Boolean = true


    companion object {
        fun getInstance() = service<ProjectOpenerSettingsState>()
    }

    override fun getState(): ProjectOpenerSettingsState {
        return this
    }

    override fun loadState(state: ProjectOpenerSettingsState) {
        XmlSerializerUtil.copyBean(state, this)
    }
}
