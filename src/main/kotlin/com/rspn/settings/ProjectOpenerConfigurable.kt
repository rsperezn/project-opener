package settings

import com.intellij.icons.AllIcons
import com.intellij.ide.impl.ProjectUtil
import com.intellij.openapi.Disposable
import com.intellij.openapi.fileChooser.FileChooser
import com.intellij.openapi.fileChooser.FileChooserDescriptorFactory
import com.intellij.openapi.options.Configurable
import com.intellij.openapi.ui.asSequence
import com.intellij.openapi.util.io.FileUtil
import com.intellij.openapi.vfs.LocalFileSystem
import com.intellij.ui.components.JBCheckBox
import com.intellij.ui.components.JBList
import com.intellij.util.ui.FormBuilder
import com.intellij.util.ui.components.BorderLayoutPanel
import com.rspn.UIBundle
import java.awt.FlowLayout
import java.io.File
import javax.swing.DefaultListModel
import javax.swing.JButton
import javax.swing.JComponent
import javax.swing.JPanel

class ProjectOpenerConfigurable : Configurable {
    private val ui = SettingsComponent(ProjectOpenerSettingsState.getInstance())
    override fun createComponent(): JComponent {
        return ui.getComponent()
    }

    override fun isModified(): Boolean {
        val settings = ProjectOpenerSettingsState.getInstance()

        var modify= ui.directories.model.asSequence().toList() != settings.projectsRootPaths || ui.forceOpenNewWindow.isSelected != settings.forceOpenNewWindows
        return modify
    }

    override fun apply() {
        val settings = ProjectOpenerSettingsState.getInstance()
        settings.projectsRootPaths = ui.directories.model.asSequence().toList()
        settings.forceOpenNewWindows = ui.forceOpenNewWindow.isSelected
    }

    override fun getDisplayName() = UIBundle.message("settings.title")

    override fun disposeUIResources() {
        super.disposeUIResources()
        ui.dispose()
    }
}

private class SettingsComponent(settingsState: ProjectOpenerSettingsState) : Disposable {

    var defaultModel = DefaultListModel<String>();

    val directories = JBList(settingsState.projectsRootPaths).apply {
        defaultModel.addAll(settingsState.projectsRootPaths)
        model = defaultModel
    }

    val forceOpenNewWindow = JBCheckBox().apply {
        isSelected = settingsState.forceOpenNewWindows
    }

    val addProjectRoot = JButton().apply {
        icon = AllIcons.General.Add
        addActionListener {
            val currentFileLocation = ProjectUtil.getBaseDir()

            val descriptor = FileChooserDescriptorFactory.createSingleFolderDescriptor()
            val toSelect =
                LocalFileSystem.getInstance().refreshAndFindFileByIoFile(File(currentFileLocation))

            FileChooser.chooseFile(descriptor, null, toSelect) { file ->
                if (file != null && file.isDirectory && file.isWritable) {
                    defaultModel.addElement(FileUtil.toSystemDependentName(file.path))
                }
            }
        }
    }

    val removeProjectRoot = JButton().apply {
        icon = AllIcons.General.Remove
        addActionListener {
            val selectedIndex = directories.selectedIndex
            if (!settingsState.projectsRootPaths.isEmpty() && selectedIndex > -1) {
                defaultModel.remove(selectedIndex)
            }
        }
    }

    val panel = JPanel(FlowLayout(FlowLayout.LEFT)).apply {
        add(addProjectRoot)
        add(removeProjectRoot)
    }

    fun getComponent(): JComponent = FormBuilder.createFormBuilder()
        .addLabeledComponent("Projects root paths", directories)
        .addComponent(panel)
        .addSeparator()
        .addLabeledComponent("Force to open projects in new windows", forceOpenNewWindow)
        .panel.let {
            BorderLayoutPanel().apply { addToTop(it) }
        }

    override fun dispose() {
    }

}