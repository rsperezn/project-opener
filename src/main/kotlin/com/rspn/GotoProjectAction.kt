package com.rspn

import com.intellij.ide.actions.GotoActionBase
import com.intellij.ide.util.gotoByName.ChooseByNameItemProvider
import com.intellij.ide.util.gotoByName.ChooseByNamePopup
import com.intellij.notification.NotificationGroupManager
import com.intellij.notification.NotificationType
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.application.ModalityState
import com.intellij.openapi.fileTypes.FileType
import com.intellij.openapi.project.DumbAware
import com.intellij.openapi.project.Project
import com.intellij.pom.Navigatable
import com.rspn.model.GotoProjectModel
import settings.ProjectOpenerSettingsState

class GotoProjectAction : GotoActionBase(), DumbAware {

    override fun gotoActionPerformed(anActionEvent: AnActionEvent) {
        val project = anActionEvent.getData(CommonDataKeys.PROJECT)

        if (project != null) {
            val gotoProjectModel = GotoProjectModel(project)
            val callback = object : GotoActionCallback<FileType>() {
                override fun elementChosen(popup: ChooseByNamePopup, element: Any?) {
                    if (element != null) {
                        ApplicationManager.getApplication().invokeLater({
                            val n = element as Navigatable?
                            n!!.navigate(true)
                        }, ModalityState.NON_MODAL)
                    }
                }
            }
            val provider = GotoProjectItemProvider(getPsiContext(anActionEvent)) as ChooseByNameItemProvider
            when {
                ProjectOpenerSettingsState.getInstance().projectsRootPaths.isEmpty() -> notifySettingsMissingIfRequired(project)
                else -> this.showNavigationPopup(anActionEvent, gotoProjectModel, callback, "Files Matching Pattern", true, false, provider)
            }
        }
    }

    private fun notifySettingsMissingIfRequired(project: Project) {
        val title = UIBundle.message("settings.title") + " Settings"
        val content = "Configure your Project Opener Settings before you can navigate to projects using the plugin";
        NotificationGroupManager.getInstance()
            .getNotificationGroup("Notification group")
            .createNotification(title, content, NotificationType.INFORMATION)
            .notify(project);
    }

    override fun update(event: AnActionEvent) {
        event.presentation.isEnabledAndVisible = true
    }
}
