package com.rspn

import com.intellij.navigation.ChooseByNameContributor
import com.intellij.navigation.NavigationItem
import com.intellij.openapi.project.Project
import settings.ProjectOpenerSettingsState
import java.nio.file.Path

class GotoProjectNameProvider internal constructor() : ChooseByNameContributor {
    private var fullyQualifiedProjects: MutableList<Path>

    init {
        val settings = ProjectOpenerSettingsState.getInstance()

        fullyQualifiedProjects = settings.projectsRootPaths
            .flatMap { FileService.getProjects(it) }
            .toMutableList()
        fullyQualifiedProjects.removeAll(settings.projectsRootPaths.map { Path.of(it) })
    }

    override fun getNames(project: Project, includeNonProjectItems: Boolean): Array<String> {
        return fullyQualifiedProjects.map { it.toFile().name }.toTypedArray()
    }

    override fun getItemsByName(
        name: String,
        pattern: String,
        project: Project,
        includeNonProjectItems: Boolean
    ): Array<NavigationItem> {
        return fullyQualifiedProjects
            .asSequence()
            .filter { it.toFile().name == name }
            .map { GotoProjectNavigationItem(it) }
            .toList()
            .toTypedArray()
    }
}
