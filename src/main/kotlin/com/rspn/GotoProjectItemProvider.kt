package com.rspn

import com.intellij.ide.util.gotoByName.DefaultChooseByNameItemProvider
import com.intellij.psi.PsiElement

class GotoProjectItemProvider internal constructor(context: PsiElement?)
    : DefaultChooseByNameItemProvider(context)
