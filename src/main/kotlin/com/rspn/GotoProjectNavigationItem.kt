package com.rspn

import com.intellij.ide.impl.ProjectUtil
import com.intellij.navigation.ItemPresentation
import com.intellij.navigation.NavigationItem
import com.intellij.openapi.project.Project
import com.intellij.openapi.project.ex.ProjectManagerEx
import com.intellij.openapi.wm.WindowManager
import settings.ProjectOpenerSettingsState
import java.nio.file.Path
import javax.swing.Icon

class GotoProjectNavigationItem internal constructor(private val projectFQN: Path)
    : NavigationItem {


    private val projectPath: String = projectFQN.toFile().absolutePath
    private val projectName: String = projectFQN.toFile().name
    private val forceOpenProjectNewWindow = ProjectOpenerSettingsState.getInstance().forceOpenNewWindows

    override fun getName(): String? {
        return projectName
    }

    override fun getPresentation(): ItemPresentation? {
        return object : ItemPresentation {
            override fun getPresentableText(): String? {
                return this@GotoProjectNavigationItem.name
            }

            override fun getLocationString(): String? {
                return this@GotoProjectNavigationItem.projectPath
            }

            override fun getIcon(b: Boolean): Icon? {
                return null
            }
        }
    }

    override fun navigate(requestFocus: Boolean) {
        val manager = ProjectManagerEx.getInstanceEx()
        val openedProjects = manager.openProjects
        val project = openedProjects.find { it.name == projectName }
        if (project != null) {
            focusOnOpenedProject(project)
            return
        }
        ProjectUtil.openOrImport(projectFQN.toFile().absolutePath, null, forceOpenProjectNewWindow)
    }

    private fun focusOnOpenedProject(openProject: Project) {
        val projectFrame = WindowManager.getInstance().getFrame(openProject)
        val frameState = projectFrame!!.extendedState
        if (frameState and 1 == 1) {
            projectFrame.extendedState = frameState xor 1
        }

        projectFrame.toFront()
        projectFrame.requestFocus()
    }

    override fun canNavigate(): Boolean {
        return true
    }

    override fun canNavigateToSource(): Boolean {
        return false
    }
}
